#!/bin/python3

import sys

keywords = ['int','return']
statements = ['return']
tokens = ['{','}','(',')',';']

INT_KEYWORD    = keywords[0]
RETURN_KEYWORD = keywords[1]

#
#
#
def verify_tokens(string):
  tk = ''
  result = []

  for s in string:
    if s in tokens:
      if len(tk) > 0:
        result.append(tk)
        result.append(s)
        tk = ''
      else:
        result.append(s)
    else:
      tk += s

  if len(tk) > 0:
    result.append(tk)

  return result

def verify_tokens1(string):
  result = []
  for t in tokens:
    if t in string:
      string = string.replace(t,'')
      result.append(t)
  
  if len(string) > 0:
    result.insert(0,string)

  return result
      
       
#
#
#
def lex(codigo):
  ret = codigo.replace('\n','').replace('\t','').split(' ')
  result = []
  for w in ret:
    if len(w) > 0:
      r = verify_tokens(w)
      for v in r:
        result.append(v)
  return result

#
#
#
def parser_expression(exp):
  return 'Const('+ exp +')'

#
#
#
def parser_statement(token):

    if token[0] != RETURN_KEYWORD:
        return 2, 'Invalid keyword: '+token[0], []

    #if  token[1]) != :
    #    return 2, 'Invalid value type: '+token[1] + '. Integer is expected!!!', []

    exp = parser_expression(token[1])

    statement, exp = 'Return(exp)', 'exp = ' + exp

    if token[2] != ';':
        return 2, 'Invalid statement, semicolon expected here!!!', []

    return 0, statement, exp, token[3:]
#
#
#
def parse_program(token):
    if token[0] != 'int':
        return 1, 'Invalid program, a function was expected here!!!'

    if token[1] != 'main':
        return 1, 'Invalid program, a main function was expected here!!!'

    if token[2] != '(':
        return 1, 'Invalid program, a "(" symbol was expected here!!!'

    if token[3] != ')':
        return 1, 'Invalid program, a ")" symbol was expected here!!!'

    if token[4] != '{':
        return 1, 'Invalid program, a "{" symbol was expected here!!!'

    if not token[5] in statements:
        return 1, 'Invalid program, a statement was expected here!!!'

    ret, statement, exp, token = parser_statement(token[5:])

    #
    # fail to parse statement?
    #
    if ret > 0:
        return 1, statement

    if token[0] != '}':
        return 1, 'Invalid program, a "}" symbol function was expected here!!!'

    return 0, ['prog = Prog(func_decl','func_decl = Fun(string, statement)','statement = ' + statement, exp]

#
#
#
def main():
  if len(sys.argv) == 1:
    print("Informe o nome do programa que deseja compilar!!!")

    return

  arquivo = sys.argv[1]

  with open(arquivo,'r') as file:
    codigo = file.read()
    tokens = lex(codigo)
    ret, ast = parse_program(tokens)
    if ret > 0:
        print ('Erro: ' + str(ret) + ' ' + ast)
        return

    for a in ast:
        print(a)


main()


#======================================
#   Documentation
#======================================
# Source: https://norasandler.com/2017/11/29/Write-a-Compiler.html
#======================================
#
# Formal grammar
#
# <program> ::= <function>
# <function> ::= "int" <id> "(" ")" "{" <statement> "}"
# <statement> ::= "return" <exp> ";"
# <exp> ::= <int>
#
#
